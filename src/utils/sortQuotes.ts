import QuoteDTO from "../models/QuoteDTO/QuoteDTO";

export function smallestLetterFirst(q1: QuoteDTO, q2: QuoteDTO): number {
	if(q1.message < q2.message) {
		return -1;
	} else {
		return 1;
	}
}

export function mostRecentFirst(q1: QuoteDTO, q2: QuoteDTO): number {
	if(q1.date < q2.date) {
		return 1;
	} else if(q1.date === q2.date) {
		return smallestLetterFirst(q1, q2);
	} else {
		return -1;
	}
}