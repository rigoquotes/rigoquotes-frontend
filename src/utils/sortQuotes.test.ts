import {mostRecentFirst, smallestLetterFirst} from "./sortQuotes";
import dummyQuotes from "./dummyQuotes";
import sortNoMutation from "./sortNoMutation";

const unsortedQuotes = dummyQuotes;

describe("La fonction mostRecentFirst", () => {
	const sortedQuotes = sortNoMutation(unsortedQuotes, mostRecentFirst);

	it("devrait afficher la quote avec la propriété date la plus élevée en premier", () => {
		expect(sortedQuotes[0]).toBe(unsortedQuotes[3]);
	});
	it("devrait afficher la quote avec la propriété date la plus faible en dernier", () => {
		expect(sortedQuotes[4]).toBe(unsortedQuotes[0]);
	});
	it("devrait trier en fonction du message de la quote si les dates sont à égalité", () => {
		expect(sortedQuotes[2]).toBe(unsortedQuotes[1]);
		expect(sortedQuotes[3]).toBe(unsortedQuotes[2]);
	});
	it("devrait afficher les quotes dans l'ordre décroissant par rapport à la propriété date", () => {
		expect(sortedQuotes[0]).toBe(unsortedQuotes[3]);
		expect(sortedQuotes[1]).toBe(unsortedQuotes[4]);
		expect(sortedQuotes[2]).toBe(unsortedQuotes[1]);
		expect(sortedQuotes[3]).toBe(unsortedQuotes[2]);
		expect(sortedQuotes[4]).toBe(unsortedQuotes[0]);
	});
});

describe("La fonction smallestLetterFirst", () => {
	const sortedQuotes = sortNoMutation(unsortedQuotes, smallestLetterFirst);

	it("devrait afficher la quote avec la propriété message contenant les lettres les plus petites en premier", () => {
		expect(sortedQuotes[0]).toBe(unsortedQuotes[4]);
	});
	it("devrait afficher la quote avec la propriété message contenant les lettres les plus grandes en dernier", () => {
		expect(sortedQuotes[4]).toBe(unsortedQuotes[3]);
	});
	it("devrait afficher les quotes dans l'ordre alphabétique par rapport aux messages", () => {
		expect(sortedQuotes[0]).toBe(unsortedQuotes[4]);
		expect(sortedQuotes[1]).toBe(unsortedQuotes[1]);
		expect(sortedQuotes[2]).toBe(unsortedQuotes[2]);
		expect(sortedQuotes[3]).toBe(unsortedQuotes[0]);
		expect(sortedQuotes[4]).toBe(unsortedQuotes[3]);
	});
});