import QuoteDTO from "../models/QuoteDTO/QuoteDTO";

export default[
	new QuoteDTO({
		id: 5,
		creator: "sixelasacul",
		message: "Le code, c'est pas comme la bite. Plus c'est petit, mieux c'est.",
		date: 1445637600000,
		authors: [
			"Paul Queruel"
		],
		promotion: "A1",
		school: "Aix-en-Provence",
		year: "2015 - 2016"
	}),
	new QuoteDTO({
		id: 2,
		creator: "poeeel",
		message: "Fast and Cyrius",
		date: 1508450400000,
		authors: [
			"Paul Queruel"
		],
		promotion: "A3",
		school: "Aix-en-Provence",
		year: "2017 - 2018"
	}),
	new QuoteDTO({
		id: 3,
		creator: "Eschyle",
		message: "Le 8 Janvier, rattrapages nationaux. Marque bien cette date François",
		date: 1508450400000,
		authors: [
			"Emmanuel Barbolini"
		],
		promotion: "A3",
		school: "Aix-en-Provence",
		year: "2017 - 2018"
	}),
	new QuoteDTO({
		id: 4,
		creator: "sixelasacul",
		message: "Y'a les B2B, les B2C, ...\nPourquoi pas les 2Be3.",
		date: 1553639642763,
		authors: [
			"Alexis Lucas",
			"Emmanuel Barbolini"
		],
		promotion: "A4",
		school: "Aix-en-Provence",
		year: "2018 - 2019"
	}),
	new QuoteDTO({
		id: 1,
		creator: "sixelasacul",
		message: "Eh, vous avez vu le rachat de Github par Microsoft ?\nOuais, je l'ai vu sur touteslesactualitésenretardde5jours.com.",
		date: 1527804000000,
		authors: [
			"Quentin Lebariller",
			"Lilian Berger"
		],
		promotion: "A3",
		school: "Aix-en-Provence",
		year: "2017 - 2018"
	})
];