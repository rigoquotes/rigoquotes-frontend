export default function sortNoMutation<T>(array: T[], sort?: ((a: T, b: T) => number) | undefined): T[] {
	return array.concat().sort(sort);
}