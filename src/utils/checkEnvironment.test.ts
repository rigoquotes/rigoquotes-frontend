import {checkEnv, Environment} from "./checkEnvironment";

describe("La fonction checkEnvironment", () => {
	/* eslint-disable no-undef */
	global.window = Object.create(window);
	Object.defineProperty(window, "location", {
		value: {
			host: ""
		}
	});
    
	it("devrait retourner DEV de l'énumération Environment pour un déploiement local", () => {
		window.location.host = "localhost:3000";

		const env = checkEnv();
		const expected = Environment.DEV;

		expect(env).toBe(expected);
	});

	it("devrait retourner STAGING de l'énumération Environment pour un déploiement de préproduction", () => {
		window.location.host = "staging.rigoquotes.fr";

		const env = checkEnv();
		const expected = Environment.STAGING;

		expect(env).toBe(expected);
	});

	it("devrait retourner PROD de l'énumération Environment pour un déploiement de production", () => {
		window.location.host = "rigoquotes.fr";

		const env = checkEnv();
		const expected = Environment.PROD;

		expect(env).toBe(expected);
	});
});
