export enum Environment {
	DEV, STAGING, PROD
}

export function checkEnv(): Environment {
	const {host} = window.location;
	
	if(host.indexOf("localhost") !== -1) {
		return Environment.DEV;
	}
	if(host.indexOf("staging") !== -1) {
		return Environment.STAGING;
	}
	return Environment.PROD;
}
