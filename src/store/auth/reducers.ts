import produce from "immer";
import {USER_PROFILE_LOADED, AuthActionTypes, AuthState} from "./types";

const initialState: AuthState = {
	authenticated: false,
	idToken: "",
	accessToken: "",
	expiresAt: 0,
	profile: {
		nickname: "",
		email: ""
	}
};

function auth(state: AuthState = initialState, action: AuthActionTypes): AuthState {
	return produce(state, draft => {
		switch(action.type) {
			case USER_PROFILE_LOADED: {
				const {result} = action.payload;
				draft.authenticated = result.authenticated;
				draft.idToken = result.idToken;
				draft.expiresAt = result.expiresAt;
				draft.profile = result.profile;
				break;
			}
			default:
				break;
		}
	});
}

export default auth;