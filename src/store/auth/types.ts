import {Profile} from "../../models/Auth/AuthResult";

export interface AuthState {
	authenticated: boolean;
	idToken: string;
	accessToken: string;
	expiresAt: number;
	profile: Profile;
}

export const HANDLE_AUTHENTICATION_CALLBACK = "HANDLE_AUTHENTICATION_CALLBACK";
export const HANDLE_SESSION_RENEWAL = "HANDLE_SESSION_RENEWAL";
export const USER_PROFILE_LOADED = "USER_PROFILE_LOADED";

interface HandleAuthenticationCallbackAction {
	type: typeof HANDLE_AUTHENTICATION_CALLBACK;
}

interface HandleSessionRenewalAction {
	type: typeof HANDLE_SESSION_RENEWAL;
}

interface LoadUserProfileAction {
	type: typeof USER_PROFILE_LOADED;
	payload: {
		result: AuthState;
	};
}

export type AuthActionTypes = HandleAuthenticationCallbackAction | LoadUserProfileAction | HandleSessionRenewalAction;
