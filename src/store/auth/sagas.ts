import {takeLatest, put, call} from "redux-saga/effects";
import {HANDLE_AUTHENTICATION_CALLBACK, HANDLE_SESSION_RENEWAL} from "./types";
import {AuthResult} from "../../models/Auth/AuthResult";
import {loadUserProfile} from "./actions";
import AuthClient from "../../models/Auth/AuthClient";

function* parseHash() {
	const response: AuthResult = yield call(AuthClient.handleAuthentication);
	console.log(response);
	yield put(loadUserProfile(response));
}

function* startSessionRenewal() {
	const response: AuthResult = yield call(AuthClient.renewSession);
	console.log(response);
	yield put(loadUserProfile(response));
}

export function* handleAuthenticationCallbackWatcher() {
	yield takeLatest(HANDLE_AUTHENTICATION_CALLBACK, parseHash);
}

export function* handleTokenRenewalWatcher() {
	yield takeLatest(HANDLE_SESSION_RENEWAL, startSessionRenewal);
}
