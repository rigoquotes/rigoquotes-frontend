import {HANDLE_AUTHENTICATION_CALLBACK, USER_PROFILE_LOADED, AuthActionTypes, HANDLE_SESSION_RENEWAL} from "./types";
import {AuthResult} from "../../models/Auth/AuthResult";

export function handleAuthenticationCallback(): AuthActionTypes {
	return {
		type: HANDLE_AUTHENTICATION_CALLBACK
	};
}

export function handleSessionRenewal(): AuthActionTypes {
	return {
		type: HANDLE_SESSION_RENEWAL
	};
}

export function loadUserProfile(result: AuthResult): AuthActionTypes {
	return {
		type: USER_PROFILE_LOADED,
		payload: {
			result
		}
	};
}