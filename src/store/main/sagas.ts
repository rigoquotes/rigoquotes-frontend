import {all} from "redux-saga/effects";
import fetchQuotesWatcher from "../quotes/sagas";
import {handleAuthenticationCallbackWatcher, handleTokenRenewalWatcher} from "../auth/sagas";

function* rootSagas() {
	yield all([
		fetchQuotesWatcher(),
		handleAuthenticationCallbackWatcher(),
		handleTokenRenewalWatcher()
	]);
}

export default rootSagas;