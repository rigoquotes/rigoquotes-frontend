import createSagaMiddleware from "@redux-saga/core";
import {createStore, applyMiddleware, AnyAction, Store} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {rootReducers, AppState} from "./reducers";
import rootSagas from "./sagas";

export default function configureStore(): Store<AppState, AnyAction> {
	const middleware = createSagaMiddleware();
	const store = createStore(
		rootReducers,
		composeWithDevTools(applyMiddleware(middleware))
	);
	middleware.run(rootSagas);
	return store;
}

