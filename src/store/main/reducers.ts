import {combineReducers} from "redux";
import quotes from "../quotes/reducers";
import auth from "../auth/reducers";

export const rootReducers = combineReducers({
	quotes,
	auth
});

export type AppState = ReturnType<typeof rootReducers>;