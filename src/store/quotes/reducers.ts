import produce from "immer";
import {FETCH_QUOTES, SET_QUOTES, QuotesState, QuotesActionTypes} from "./types";

const initialState: QuotesState = {
	isFetching: false,
	error: undefined,
	quotes: []
};

function quotes(state: QuotesState = initialState, action: QuotesActionTypes): QuotesState {
	return produce(state, draft => {
		switch(action.type) {
			case FETCH_QUOTES:
				draft.isFetching = true;
				break;
			case SET_QUOTES:
				draft.quotes = action.payload.quotes;
				draft.error = action.payload.error;
				draft.isFetching = false;
				break;
			default:
				break;
		}
	});
}

export default quotes;