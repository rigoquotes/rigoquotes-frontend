import {QuotesActionTypes, FETCH_QUOTES, SET_QUOTES} from "./types";
import QuoteDTO from "../../models/QuoteDTO/QuoteDTO";

export function fetchQuotes(): QuotesActionTypes {
	return {
		type: FETCH_QUOTES
	};
}

export function setQuotes(quotes: QuoteDTO[], error: Error): QuotesActionTypes {
	return {
		type: SET_QUOTES,
		payload: {
			quotes,
			error
		}
	};
}
