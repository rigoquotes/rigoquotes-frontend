import QuoteDTO from "../../models/QuoteDTO/QuoteDTO";

export interface QuotesState {
	quotes: QuoteDTO[];
	error?: Error;
	isFetching: boolean;
}

export const FETCH_QUOTES = "FETCH_QUOTES";
export const SET_QUOTES = "SET_QUOTES";

interface FetchQuotesAction {
	type: typeof FETCH_QUOTES;
}

interface SetQuotesAction {
	type: typeof SET_QUOTES;
	payload: {
		quotes: QuoteDTO[];
		error: Error;
	};
}

export type QuotesActionTypes = FetchQuotesAction | SetQuotesAction;
