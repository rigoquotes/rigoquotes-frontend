import {put, takeLatest, delay} from "redux-saga/effects";
import {FETCH_QUOTES} from "../quotes/types";
import dummyQuotes from "../../utils/dummyQuotes";
import QuoteDTO from "../../models/QuoteDTO/QuoteDTO";
import {setQuotes} from "./actions";

const getQuotes = () => new Promise((resolve, reject) => {
	const doesSucceed = Math.random() > 0.1 ? true : false;
	const data = dummyQuotes;
	const error = new Error("Couldn't get data");
	doesSucceed ? resolve(data) : reject(error);
});

function* fetchQuotes() {
	const timeout = Math.round(Math.random() * (2 - 0.1) + 0.1);
	yield delay(timeout * 1000);

	let quotes = new Array<QuoteDTO>();
	let error = null;

	try{
		quotes = yield getQuotes();
	} catch(e) { 
		error = e;
	}
	
	yield put(setQuotes(quotes, error));
}

function* fetchQuotesWatcher() {
	yield takeLatest(FETCH_QUOTES, fetchQuotes);
}

export default fetchQuotesWatcher;