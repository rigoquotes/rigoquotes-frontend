import "dayjs/locale/fr";
import dayjs from "dayjs";

dayjs.locale("fr");

export interface QuoteInterface {
	id: number;
	date: number;
	message: string;
	creator: string;
	authors: string[];
	promotion: string;
	school: string;
	year: string;
}

export default class QuoteDTO implements QuoteInterface {
	id: number;
	date: number;
	message: string;
	creator: string;
	authors: string[];
	promotion: string;
	school: string;
	year: string;
	
	constructor(quote?: QuoteInterface) {
		this.id = quote && quote.id || 0;
		this.date = quote && quote.date || Date.now();
		this.message = quote && quote.message || "";
		this.creator = quote && quote.creator || "";
		this.authors = quote && quote.authors || [""];
		this.promotion = quote && quote.promotion || "";
		this.school = quote && quote.school || "";
		this.year = quote && quote.year || "";
	}

	getDisplayDate(): string {
		return dayjs(this.date).format("D MMMM YYYY");
	}

	getAuthorsInString(): string {
		return this.authors.join(", ").toString();
	}

	getFormatedQuote(): string {
		if(this.message.indexOf("\n") !== -1) {
			return "- " + this.message.replace("\n", "\n- ");
		}
		return this.message;
	}
}
