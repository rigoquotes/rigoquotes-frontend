import auth0 from "auth0-js";
import {AuthResult} from "./AuthResult";
import {Environment, checkEnv} from "../../utils/checkEnvironment";

class AuthClient {
	private webAuth: auth0.WebAuth;
	private environment: Environment;
	private tokenRenewalTimeout: NodeJS.Timeout;
	private tokenExpiresAt: number;
	private protocol: string;
	private domain: string;
	private baseDomain: string;
	private clientID: string;
	private audience: string;
	private redirectUri: string;
	private responseType: string[];
	private scope: string[];

	constructor() {
		this.tokenExpiresAt = 0;
		this.clientID = "3D7V31D8lbKj9jn1mLdczbjsaFGGeQj3";
		this.audience = "https://api.rigoquotes.fr/";
		this.responseType = ["token", "id_token"];
		this.scope = ["openid", "profile", "email"];
		this.baseDomain = window.location.host;
		this.environment = checkEnv();
		this.protocol = this.computeProtocol();
		this.domain = this.computeDomain();
		this.redirectUri = `${this.protocol}://${this.baseDomain}`;
		this.webAuth = new auth0.WebAuth({
			domain: this.domain,
			audience: this.audience,
			clientID: this.clientID,
			redirectUri: `${this.redirectUri}/callback`,
			responseType: this.responseType.join(" "),
			scope: this.scope.join(" ")
		});
		this.tokenRenewalTimeout = setTimeout(() => {}, 0);
		this.scheduleRenewal();
	}

	private computeProtocol(): string {
		return this.environment === Environment.DEV ? "http" : "https";
	}
	
	private computeDomain(): string {
		return this.environment === Environment.PROD ? "auth.rigoquotes.fr" : "rigoquotes.eu.auth0.com";
	}

	private scheduleRenewal(): void {
		const timeout: number = this.tokenExpiresAt - Date.now();
		if(timeout > 0) {
			this.tokenRenewalTimeout = setTimeout(() => {
				this.renewSession();
			}, timeout);
		}
	}

	public logIn = (): void => {
		this.webAuth.authorize();
	}
	
	public logOut = (): void => {
		localStorage.removeItem("isLoggedIn");
		clearTimeout(this.tokenRenewalTimeout);
		this.webAuth.logout({
			returnTo: `${this.redirectUri}`,
			clientID: this.clientID
		});
	}
	
	public setSession = (): void => {
		localStorage.setItem("isLoggedIn", "true");
		this.scheduleRenewal();
	}
	
	public isSignedIn = (): boolean => {
		const value = localStorage.getItem("isLoggedIn");
		if(value === null || value === undefined || value === "false") {
			return false;
		}
		return true;
	}
	
	public handleAuthentication = (): Promise<AuthResult> => {
		return new Promise((resolve, reject) => {
			this.webAuth.parseHash({}, (err, authResult) => {
				if(err)return reject(err);
				if(!authResult || !authResult.idToken || !authResult.accessToken) {
					return reject(new Error("Invalid authResult or token."));
				}
				const formatedAuthResult = new AuthResult(authResult);
				this.tokenExpiresAt = formatedAuthResult.expiresAt;
				this.setSession();
				resolve(formatedAuthResult);
			});
		});
	}
	
	public renewSession = (): Promise<AuthResult> => {
		return new Promise((resolve, reject) => {
			this.webAuth.checkSession({}, (err, authResult) => {
				if(err) {
					return reject(err);
				}
				if(!authResult || !authResult.idToken || !authResult.accessToken) {
					return reject(new Error("Couldn't renew the token."));
				}
				const formatedAuthResult = new AuthResult(authResult);
				this.tokenExpiresAt = formatedAuthResult.expiresAt;
				this.setSession();
				resolve(formatedAuthResult);
			});
		});
	}
}

export default new AuthClient();
