import {Auth0DecodedHash} from "auth0-js";
import {AuthState} from "../../store/auth/types";

export interface ProfileInterface {
	email: string;
	nickname: string;
}

export class Profile implements ProfileInterface {
	readonly email: string;
	readonly nickname: string;

	constructor(payload?: ProfileInterface) {
		this.email = payload && payload.email || "";
		this.nickname = payload && payload.nickname || "";
	}
}

export class AuthResult implements AuthState {
	readonly authenticated: boolean;
	readonly idToken: string;
	readonly accessToken: string;
	readonly expiresAt: number;
	readonly profile: Profile;

	constructor(result?: Auth0DecodedHash) {
		this.authenticated = true;
		if(result && result.idToken) {
			this.idToken = result && result.idToken;
			this.accessToken = (result && result.accessToken) || "";
			this.expiresAt = result && result.idTokenPayload.exp * 1000;
			this.profile = result && new Profile(result.idTokenPayload);
		} else {
			this.idToken = "";
			this.accessToken = "";
			this.expiresAt = 0;
			this.profile = new Profile();
		}
	}
}