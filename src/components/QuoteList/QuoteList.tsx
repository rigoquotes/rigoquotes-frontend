import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import Quote from "../Quote/Quote";
import {fetchQuotes} from "../../store/quotes/actions";
import {mostRecentFirst} from "../../utils/sortQuotes";
import QuoteDTO from "../../models/QuoteDTO/QuoteDTO";
import {QuotesState} from "../../store/quotes/types";
import {AppState} from "../../store/main/reducers";

interface QuoteListProps {
	isFetching: boolean;
	quotes: QuoteDTO[];
	fetchQuotes: typeof fetchQuotes;
	error?: Error;
}

export class QuoteList extends Component<QuoteListProps> {
	static defaultProps = {
		quotes: []
	};

	componentDidMount() {
		if(this.props.quotes.length === 0) {
			this.props.fetchQuotes();
		}
	}

	render() {
		const {quotes, isFetching, error} = this.props;
		if(quotes.length > 0) {
			return (
				<Fragment>
					{quotes.sort(mostRecentFirst).map((quote) =>
						<Quote quote={quote} fullDisplay={false} key={quote.id} />
					)}
				</Fragment>
			);
		}
		if(isFetching) {
			return <p>Chargement...</p>;
		}
		if(error) {
			return <p>Erreur lors du chargement des rigoquotes.</p>;
		}
		return <Fragment />;
	}
}

function mapStateToProps(state: AppState): QuotesState {
	return state.quotes;
}

const mapDispatchToProps = {
	fetchQuotes
};

export default connect(mapStateToProps, mapDispatchToProps)(QuoteList);