import React, {Component, Fragment} from "react";
import {connect} from "react-redux";
import {AppState} from "../../store/main/reducers";
import {AuthState} from "../../store/auth/types";
import {Profile} from "../../models/Auth/AuthResult";
import {handleSessionRenewal} from "../../store/auth/actions";
import AuthClient from "../../models/Auth/AuthClient";

interface AuthProps {
	authenticated: boolean;
	profile: Profile;
	handleSessionRenewal: typeof handleSessionRenewal;
}

export class Auth extends Component<AuthProps> {
	componentDidMount() {
		if(!this.props.authenticated && AuthClient.isSignedIn()) {
			this.props.handleSessionRenewal();
		}		
	}

	render() {
		const {authenticated, profile} = this.props;
		return (
			<div>
				{ authenticated ?
					(
						<Fragment>
							<p>{profile.nickname}</p>
							<p>{profile.email}</p>
							<button onClick={AuthClient.logOut}>Logout</button>
						</Fragment>
					) : (
						<Fragment>
							<p>Authentication</p>
							<button onClick={AuthClient.logIn}>Login</button>
						</Fragment>
					)}
				<hr />
			</div>
		);
	}
}

function mapStateToProps(state: AppState): AuthState {
	return state.auth;
}

const mapDispatchToProps = {
	handleSessionRenewal
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);