import React, {Component, Fragment} from "react";
import {Link} from "react-router-dom";

class Home extends Component {
	render(): JSX.Element {
		return (
			<Fragment>
				<p>Bienvenue sur Rigochaos.</p>
				<button>
					<Link to="/quotes">Voir les quotes</Link>
				</button>
			</Fragment>
		);
	}
}

export default Home;