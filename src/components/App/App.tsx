import React, {Component} from "react";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import QuoteList from "../QuoteList/QuoteList";
import FullQuote from "../FullQuote/FullQuote";
import Auth from "../Auth/Auth";
import Callback from "../Callback/Callback";
import Home from "../Home/Home";

export default class App extends Component {
	render() {
		return (
			<BrowserRouter>
				<Auth />
				<Switch>
					<Route path="/" exact component={Home} />
					<Route path="/quotes" exact component={QuoteList} />
					<Route path="/quotes/:id" component={FullQuote} />
					<Route path="/callback" component={Callback} />
				</Switch>
			</BrowserRouter>
		);
	}
}
