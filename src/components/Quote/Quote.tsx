import React, {Component, Fragment} from "react";
import {Link} from "react-router-dom";
import QuoteDTO from "../../models/QuoteDTO/QuoteDTO";

interface QuoteProps {
	quote: QuoteDTO;
	fullDisplay: boolean;
}

class Quote extends Component<QuoteProps> {
	render() {
		const {quote, fullDisplay} = this.props;
		return (
			<dl>
				<dt>Quote</dt>
				<dd>{quote.getFormatedQuote()}</dd>
				<dt>Auteurs</dt>
				<dd>{quote.getAuthorsInString()}</dd>
				<dt>Promotion</dt>
				<dd>{quote.promotion}</dd>
				<dt>Année</dt>
				<dd>{quote.year}</dd>
				<dt>Centre</dt>
				<dd>{quote.school}</dd>
				{fullDisplay ? (
					<Fragment>
						<dt>Créateur</dt>
						<dd>{quote.creator}</dd>
						<dt>Date formatée</dt>
						<dd>{quote.getDisplayDate()}</dd>
					</Fragment>
				) : (
					<Link to={{
						pathname: `/quotes/${quote.id}`,
						state: {quote, fullDisplay: true}
					}}>Voir plus</Link>
				)}
			</dl>
		);
	}
}

export default Quote;