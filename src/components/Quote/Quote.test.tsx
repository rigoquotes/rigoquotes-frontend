import React from "react";
import {shallow, ShallowWrapper, HTMLAttributes} from "enzyme";
import Quote from "./Quote";
import dummyQuotes from "../../utils/dummyQuotes";
import QuoteDTO from "../../models/QuoteDTO/QuoteDTO";

/* eslint-disable @typescript-eslint/no-explicit-any */
function testBasicDisplay(dds: ShallowWrapper<HTMLAttributes, any, React.Component<{}, {}, any>>,
	quote: QuoteDTO) {
	expect(dds.at(0).text()).toBe(quote.getFormatedQuote());
	expect(dds.at(1).text()).toBe(quote.getAuthorsInString());
	expect(dds.at(2).text()).toBe(quote.promotion);
	expect(dds.at(3).text()).toBe(quote.year);
	expect(dds.at(4).text()).toBe(quote.school);
}

/* eslint-disable @typescript-eslint/no-explicit-any */
function testAdditionalDisplay(dds: ShallowWrapper<HTMLAttributes, any, React.Component<{}, {}, any>>,
	quote: QuoteDTO) {
	expect(dds.at(5).text()).toBe(quote.creator);
	expect(dds.at(6).text()).toBe(quote.getDisplayDate());
}

describe("Le composant Quote", () => {
	it("devrait afficher toutes les informations formatées de la citation si la propriété fullDisplay est vraie", () => {
		const currentQuote = dummyQuotes[0];
		const wrapper = shallow(
			<Quote
				quote={currentQuote}
				fullDisplay
			/>
		);

		const dl = wrapper.find("dl");
		expect(dl.exists()).toBe(true);

		const dts = dl.find("dt");
		const dds = dl.find("dd");
		expect(dts.length).toBe(7);
		expect(dds.length).toBe(7);

		testBasicDisplay(dds, currentQuote);
		testAdditionalDisplay(dds, currentQuote);
	});
	it("devrait afficher toutes les informations formatées de la citation sauf le créateur et la date de création si la propriété fullDisplay est fausse", () => {
		const currentQuote = dummyQuotes[0];
		const wrapper = shallow(
			<Quote
				quote={currentQuote}
				fullDisplay={false}
			/>
		);

		const dl = wrapper.find("dl");
		expect(dl.exists()).toBe(true);

		const dts = dl.find("dt");
		const dds = dl.find("dd");
		expect(dts.length).toBe(5);
		expect(dds.length).toBe(5);

		testBasicDisplay(dds, currentQuote);
	});
});