import React, {Component} from "react";
import Quote from "../Quote/Quote";
import {RouteComponentProps} from "react-router";

class FullQuote extends Component<RouteComponentProps> {
	render() {
		const {quote, fullDisplay} = this.props.location.state;
		return (
			<Quote quote={quote} fullDisplay={fullDisplay} />
		);
	}
}

export default FullQuote;