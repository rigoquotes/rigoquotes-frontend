import React, { Component } from 'react';
import Quote from '../Quote/Quote';

class FullQuote extends Component {
	render() {
		const { quote, fullDisplay } = this.props.location.state
		console.log(quote);
		console.log(fullDisplay);
		return (
			<Quote quote={quote} fullDisplay={fullDisplay} />
		);
	}
}

export default FullQuote;