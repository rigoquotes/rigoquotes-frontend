import React, {Component} from "react";
import {connect} from "react-redux";
import {Redirect} from "react-router";
import {AppState} from "../../store/main/reducers";
import {AuthState} from "../../store/auth/types";
import {handleAuthenticationCallback} from "../../store/auth/actions";

interface CallbackProps {
	authenticated: boolean;
	handleAuthenticationCallback: typeof handleAuthenticationCallback;
}

export class Callback extends Component<CallbackProps> {
	render(): JSX.Element {
		const {authenticated, handleAuthenticationCallback} = this.props;
		if(authenticated) {
			return <Redirect to="/" />;
		}

		handleAuthenticationCallback();
		return <div className="text-center">Loading user profile.</div>;
	}
}

function mapStateToProps(state: AppState): AuthState {
	return state.auth;
}

const mapDispatchToProps = {
	handleAuthenticationCallback
};

export default connect(mapStateToProps, mapDispatchToProps)(Callback);