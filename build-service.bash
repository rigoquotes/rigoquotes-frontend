#!/usr/bin/env bash
set -e

FG_LIGHT_GREEN="\e[92m"
FG_RESET="\e[39m"

function print() {
	echo -e "$FG_LIGHT_GREEN\$ $@$FG_RESET"
}

function run() {
	print "$@"
	"$@"
}

run docker pull $IMAGE_FULL || echo "Image not found"
run docker build . -t $PROJECT_GROUP/$SERVICE_NAME:$PROJECT_VERSION
run docker tag $PROJECT_GROUP/$SERVICE_NAME:$PROJECT_VERSION $IMAGE_FULL || echo "Image not found"
run docker push $IMAGE_FULL || echo "Image not found"
